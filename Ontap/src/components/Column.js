import React, {useCallback, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {Add} from '@material-ui/icons';
import Task from './Task';

export default function Column(props: Props) {
  const [value, setValue] = useState('');
  const arr = props.tasks;
  return (
    <View style={styles.ColumnContainer}>
      <View style={styles.mainContent}>
        <Text style={styles.ColumnTime}>{props.time}</Text>
        <Text style={styles.ColumnTitle}>{props.title}</Text>
        <FlatList
          data={arr}
          keyExtractor={(item) => item.id}
          renderItem={({item}) => (
            <Task
              id={item.id}
              content={item.content}
              time={item.time}
              onDeleteChirl={(columnId, id) => props.onDeleteItem(props.id, id)}
              onEditChirl={(columnId, id) => props.onEditItem(props.id, id)}
            />
          )}
        />

        <View style={styles.textInputContainer}>
          <TextInput
            style={styles.textInput}
            multiline={true}
            onChangeText={(value) => setValue(value)}
            placeholder={'Nhập tên công việc'}
            value={value}
          />

          <TouchableOpacity
            onPress={(item, id) => {
              props.onAddItem(value, props.id), setValue('');
            }}
            style={styles.btnAddTask}>
            <Add
              style={{
                height: 20,
                width: 20,
                padding: 10,
                lineHeight: '100%',
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContent: {
    width: 320,
    border: '1px solid #e1e4e8',
    borderRadius: 5,
    backgroundColor: '#f7f8fe',
    padding: 10,
    height: 'fit-content',
    marginHorizontal: 5,
    fontFamily: 'Segoe UI',
  },
  ColumnTime: {
    fontSize: 12,
    color: '#828080',
  },
  ColumnTitle: {
    fontSize: 16,
    fontWeight: 500,
    color: '#222',
  },
  textInputContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  textInput: {
    flex: 5,
    minHeight: '7%',
    fontSize: 14,
    padding: 10,
    border: '1px solid #ccc',
    marginRight: 10,
    backgroundColor: '#fff',
  },
  btnAddTask: {
    border: '1px solid #ccc',
    flex: 1,
    borderRadius: 5,
    alignItems: 'center',
    padding: '10px 5px',
  },
});
