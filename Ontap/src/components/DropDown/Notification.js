import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {Close} from '@material-ui/icons';

type Props = {
  close?: Function,
};

export default function Notification(props: Props) {
  const {close} = props;
  return (
    <View style={styles.Notification}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.Notification_title}>Thông báo</Text>
        <TouchableOpacity onPress={close}>
          <Close style={{height: 20, width: 20}} />
        </TouchableOpacity>
      </View>

      <View style={styles.Notification_content}>
        <Text style={{fontWeight: 'bold'}}>Không có thông báo</Text>
      </View>
      <View style={styles.Notification_buttom}>
        <TouchableOpacity>
          <Text>Thay Đổi Tần Suất Thông Báo Qua Email</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text>Cho phép thông báo trên Desktop</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Notification: {
    width: 300,
    zIndex: 1000,
    padding: 20,
  },
  Notification_title: {
    textAlign: 'center',
    paddingBottom: 10,
    flex: 5,
  },
  Notification_content: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    minHeight: 200,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#ccc',
    marginBottom: 10,
  },
  Notification_buttom: {
    borderTop: 1,
  },
});
