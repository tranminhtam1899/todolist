import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {Close, TableChart, PeopleOutline, Assessment} from '@material-ui/icons';

type Props = {
  close?: Function,
};

export default function Adds(props: Props) {
  const {close} = props;
  return (
    <View style={styles.Notification}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.Notification_title}>Tạo mới</Text>
        <TouchableOpacity onPress={close}>
          <Close style={{height: 20, width: 20}} />
        </TouchableOpacity>
      </View>

      <View style={styles.Notification_content}>
        <TouchableOpacity style={styles.mainContent}>
          <View style={styles.title}>
            <TableChart style={{height: 13, width: 13, alignSelf: 'center'}} />
            <Text style={styles.addTitle}>Tạo bảng</Text>
          </View>
          <View>
            <Text style={styles.addContent}>
              Một bảng được tạo thành từ các thẻ được sắp xếp trong danh sách.
              Sử dụng bảng để quản lý các dự án, theo dõi thông tin, hoặc tổ
              chức bất cứ việc gì.
            </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={styles.mainContent}>
          <View style={styles.title}>
            <TableChart style={{height: 13, width: 13, alignSelf: 'center'}} />
            <Text style={styles.addTitle}>Bắt đầu với mẩu</Text>
          </View>
          <View>
            <Text style={styles.addContent}>
              Bắt đầu nhanh hơn với mẫu bảng. Bắt đầu nhanh hơn với mẫu bảng.
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.mainContent}>
          <View style={styles.title}>
            <PeopleOutline
              style={{height: 13, width: 13, alignSelf: 'center'}}
            />
            <Text style={styles.addTitle}>Tạo nhóm</Text>
          </View>
          <View>
            <Text style={styles.addContent}>
              Một nhóm là tập hợp các bảng và mọi người. Sử dụng nhóm để tổ chức
              công ty của bạn, hỗ trợ người bận rộn, gia đình hoặc bạn bè.
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.mainContent}>
          <View style={styles.title}>
            <Assessment style={{height: 13, width: 13, alignSelf: 'center'}} />
            <Text style={styles.addTitle}>Tạo nhóm business</Text>
          </View>
          <View>
            <Text style={styles.addContent}>
              Với Business Class nhóm của bạn có nhiều kiểm soát an ninh, hành
              chính hơn và Power-Up không giới hạn.
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Notification: {
    width: 300,
    zIndex: 1000,
    padding: 20,
  },
  Notification_title: {
    textAlign: 'center',
    paddingBottom: 10,
    flex: 5,
  },
  Notification_content: {
    borderTopWidth: 1,
    minHeight: 200,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#ccc',
    marginBottom: 10,
  },
  title: {
    flexDirection: 'row',
  },
  addTitle: {
    fontSize: 14,
    fontWeight: 400,
    lineHeight: 20,
    paddingHorizontal: 5,
    color: '#363839',
  },
  addContent: {
    color: '#5e6c84',
    fontSize: 12,
    fontWeight: 400,
    lineHeight: 16,
    marginVertical: 7,
    padding: 0,
    float: 'left',
  },
});
