import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

type Props = {};

export default function RightMenu(props: Props) {
  return (
    <View style={styles.RightMenu}>
      <Text>rightMenu</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  RightMenu: {
    top: 0,
    right: 0,
    height: 500,
    width: 300,
  },
});
