import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {Close} from '@material-ui/icons';

type Props = {
  close?: Function,
};

export default function Info(props: Props) {
  const {close} = props;
  return (
    <View style={styles.Notification}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.Notification_title}>Thông tin</Text>
        <TouchableOpacity onPress={close}>
          <Close style={{height: 20, width: 20}} />
        </TouchableOpacity>
      </View>

      <View style={styles.Notification_content}>
        <View>
          <Image
            style={{width: '100%', height: 150}}
            source={{
              uri:
                'https://a.trellocdn.com/prgb/dist/images/tips/info-image-02@1x.d554cbf6d240549b8ef0.png',
            }}
          />
          <TouchableOpacity>
            <Text style={{textAlign: 'center', fontWeight: 500, fontSize: 14}}>
              Dễ dàng tập hợp nhóm của bạn và hoạt động với Trello Sổ chiến lược
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={{alignItems: 'center'}}>
          <Text style={styles.Notification_buttom_title}>Xem một mẹo mới</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.Notification_buttom}>
        <View style={{flex: 3, flexDirection: 'row'}}>
          <TouchableOpacity>
            <Text style={styles.Notification_buttom_title}>Biểu phí</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.Notification_buttom_title}>Ứng dụng</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 3, flexDirection: 'row'}}>
          <TouchableOpacity>
            <Text style={styles.Notification_buttom_title}>Blog</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.Notification_buttom_title}>
              Chính sách bảo mật
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View>
        <TouchableOpacity style={{alignItems: 'center'}}>
          <Text style={styles.Notification_buttom_title}>Thêm</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Notification: {
    width: 350,
    zIndex: 1000,
    padding: 20,
  },
  Notification_title: {
    textAlign: 'center',
    paddingBottom: 10,
    flex: 5,
  },
  Notification_content: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    minHeight: 200,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#ccc',
    marginBottom: 10,
  },
  Notification_buttom: {
    borderTop: 1,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  Notification_buttom_title: {
    paddingRight: 15,
    textDecoration: 'underline',
    color: '#7a869a',
    paddingVertical: 10,
  },
});
