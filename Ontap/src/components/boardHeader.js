import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {
  Assessment,
  StarBorder,
  Lock,
  CalendarToday,
  LocalPizza,
  MoreHoriz,
  Notifications,
} from '@material-ui/icons';
import {TouchableOpacity} from 'react-native-web';
import Notification from './DropDown/Notification';
import {DropdownContainer} from 'react-universal-ui';
import RightMenu from './DropDown/rightMenu';
type Props = {};

export default function BoardHeader(props: Props) {
  return (
    <View style={styles.boardHeader}>
      <View style={styles.boardHeader_left}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity style={styles.boardHeader_rightItem}>
            <Assessment style={{width: 25, height: 25, color: '#fff'}} />
          </TouchableOpacity>
          <Text style={styles.boardHeader_title}>TODO</Text>
          <TouchableOpacity style={styles.boardHeader_rightItem}>
            <StarBorder style={{width: 25, height: 25, color: '#fff'}} />
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity style={styles.boardHeader_rightItem}>
            <Text style={styles.boardHeader_title}>Cá nhân</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.boardHeader_rightItem}>
            <Lock style={{width: 15, height: 15, color: '#fff'}} />
            <Text style={styles.boardHeader_title}>Riêng tư</Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity style={styles.boardHeader_rightItem}>
            <Text style={styles.boardHeader_title}>Mời</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.boardHeader_right}>
        <TouchableOpacity style={styles.boardHeader_rightItem}>
          <CalendarToday style={{width: 15, height: 15, color: '#fff'}} />
          <Text style={styles.boardHeader_title}>Lịch</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.boardHeader_rightItem}>
          <LocalPizza style={{width: 15, height: 15, color: '#fff'}} />
          <Text style={styles.boardHeader_title}>Butler</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.boardHeader_rightItem,{marginTop:5}]}>
          <MoreHoriz style={{width: 25, height: 25, color: '#fff'}} />
          <Text style={styles.boardHeader_title}>Hiện menu</Text>
        </TouchableOpacity>

      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  boardHeader: {
    flexDirection: 'row',
    marginBottom: 10,
    fontFamily:
      "'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans', 'Helvetica Neue', Arial, sans-serif !important",

  },
  boardHeader_left: {
    flex: 3,
    flexDirection: 'row',
  },
  boardHeader_right: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  boardHeader_rightItem: {
    backgroundColor: '#ffffff4a',
    margin: 5,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 5,
    height: 35,
  },
  boardHeader_title: {
    color: '#fff',
    paddingHorizontal: 5,
    fontWeight: 500,
    alignSelf: 'center',
  },
});
