import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {
  Apps,
  Home,
  Assessment,
  Search,
  Add,
  InfoOutlined,
  Notifications,
  AccountCircle,
} from '@material-ui/icons';
import {TextInput} from 'react-native-web';
import {DropdownContainer} from 'react-universal-ui';
import Notification from './DropDown/Notification';
import Info from './DropDown/info';
import Adds from './DropDown/add';
import User from './DropDown/user';

type Props = {};

export default function Header(props: Props) {
  const [value, onChangeText] = React.useState('');
  return (
    <View style={styles.header}>
      <View style={styles.header_left}>
        <TouchableOpacity style={styles.header_icons}>
          <Apps style={{width: 25, height: 25, color: '#fff'}} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.header_icons}>
          <Home style={{width: 25, height: 25, color: '#fff'}} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.header_icons}>
          <Assessment style={{width: 25, height: 25, color: '#fff'}} />
          <Text style={{fontWeight: 500, color: '#fff'}}>Bảng</Text>
        </TouchableOpacity>
        <View style={{position: 'relative'}}>
          <TextInput
            style={styles.headerInput}
            onChangeText={(text) => onChangeText(text)}
            value={value}
          />
          <Search
            style={{
              width: 20,
              height: 20,
              color: '#fff',
              position: 'absolute',
              top: 12,
              right: 10,
            }}
          />
        </View>
      </View>
      <View style={styles.header_content}>
        <Text style={styles.header_content_title}>Quản lý công việc</Text>
      </View>
      <View style={styles.header_right}>
        <DropdownContainer
          dropdownComponent={Adds}
          dropdownOffset={{top: 0, left: 0}}>
          <View style={styles.header_icons}>
            <Add style={{width: 25, height: 25, color: '#fff'}} />
          </View>
        </DropdownContainer>
        <DropdownContainer
          dropdownComponent={Info}
          dropdownOffset={{top: 0, left: 0}}>
          <View style={styles.header_icons}>
            <InfoOutlined style={{width: 25, height: 25, color: '#fff'}} />
          </View>
        </DropdownContainer>
        <DropdownContainer
          dropdownComponent={Notification}
          dropdownOffset={{top: 0, left: 0}}>
          <View style={styles.header_icons}>
            <Notifications style={{width: 25, height: 25, color: '#fff'}} />
          </View>
        </DropdownContainer>

        <DropdownContainer
          dropdownComponent={User}
          dropdownOffset={{top: 0, left: 0}}>
          <View>
            <AccountCircle
              style={{width: 35, height: 35, color: '#fff', margin: 5}}
            />
          </View>
        </DropdownContainer>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#026aa7',
    alignItems: 'center',
    flexDirection: 'row',
    fontFamily: 'Segoe UI',
  },
  header_left: {
    flex: 2,
    flexDirection: 'row',
  },
  header_content: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header_content_title: {
    color: '#fff',
    alignItems: 'center',
    fontSize: 20,
    fontWeight: 500,
  },
  header_right: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  header_icons: {
    backgroundColor: '#ffffff4a',
    padding: 5,
    margin: 5,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  headerInput: {
    height: 35,
    borderColor: 'gray',
    borderWidth: 1,
    backgroundColor: '#ffffff5e',
    position: 'relative',
    justifyContent: 'center',
    marginTop: 5,
    padding: 5,
    border: 'none',
    borderRadius: 5,
  },
});
