import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Delete, Edit} from '@material-ui/icons';

const Task = (props) => (
  <View style={styles.taskWrapper}>
    <View>
      <Text style={styles.taskTime}>{props.time}</Text>
      <Text style={styles.task}>{props.content}</Text>
    </View>
    <View style={{marginLeft: 'auto', flexDirection: 'row'}}>
      <TouchableOpacity onPress={(columnId, id) => props.onEditChirl(null, props.id)}>
        <Edit
          style={{
            marginLeft: 'auto',
            height: 20,
            width: 20,
            cursor: 'pointer',
            padding: 5,
            color: '#fad037',
          }}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={(columnId, id) => props.onDeleteChirl(null, props.id)}>
        <Delete
          style={{
            marginLeft: 'auto',
            height: 20,
            width: 20,
            cursor: 'pointer',
            padding: 5,
            color: '#fc4c4c',
          }}
          name="trash-2"
        />
      </TouchableOpacity>
    </View>
  </View>
);

export default Task;

const styles = StyleSheet.create({
  taskWrapper: {
    backgroundColor: '#fff',
    boxShadow: '0 3px 7px 0 rgba(110,142,247,.13)',
    border: '1px solid #e1e2e8',
    borderRadius: 3,
    padding: 10,
    marginTop: 15,
    flexDirection: 'row',
    fontFamily: 'Segoe UI',
  },
  taskTime: {
    fontSize: 12,
    color: '#ccc',
  },
  verticalLine: {
    borderBottomColor: 'white',
    borderBottomWidth: 4,
    marginLeft: 10,
    width: '100%',
    position: 'absolute',
    marginTop: 15,
  },
});
