import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Modal,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import {
  ArrowForward,
  Label,
  PermIdentity,
  QueryBuilder,
  Save,
} from '@material-ui/icons';

type Props = {};

export default function ModalEdit(props: Props) {
  const [inputValue, setInputValue] = useState(props.textModal.toString());
  useEffect(() => {
    setInputValue(props.textModal.toString());
  }, [props.textModal]);

  return (
    <View style={styles.container}>
      <View>
        <Modal
          style={styles.overplay}
          animationType="slide"
          transparent={true}
          visible={props.modalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View style={{flex: 3}}>
                <TextInput
                  multiline
                  numberOfLines={4}
                  style={{
                    height: 40,
                    borderColor: 'gray',
                    borderWidth: 1,
                    padding: 10,
                    backgroundColor: '#fff',
                    borderRadius: 5,
                  }}
                  onChangeText={(text) => setInputValue(text)}
                  value={inputValue}
                />

                <View
                  style={{flexDirection: 'row', alignContent: 'space-between'}}>
                  <TouchableHighlight
                    style={styles.btnCapNhat}
                    onPress={(columnId, id, editText) =>
                      props.onUpdateItem(props.idCo, props.idTa, inputValue)
                    }>
                    <Text style={styles.textStyle}>Cập nhật</Text>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={styles.btnHuy}
                    onPress={props.CloseModal}>
                    <Text style={styles.textStyle}>Hủy</Text>
                  </TouchableHighlight>
                </View>
              </View>
              <View style={{flex: 3, marginLeft: 20}}>
                <View>
                  <TouchableOpacity style={styles.btnQuickEdit}>
                    <Label
                      style={{
                        height: 20,
                        width: 20,
                        lineHeight: '100%',
                        color: '#fff',
                        paddingHorizontal: 10,
                        paddingVertical: 5,
                      }}
                    />
                    <Text style={styles.quickEditText}>Chỉnh sửa nhãn</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.btnQuickEdit}>
                    <PermIdentity
                      style={{
                        height: 20,
                        width: 20,
                        lineHeight: '100%',
                        color: '#fff',
                        paddingHorizontal: 10,
                        paddingVertical: 5,
                      }}
                    />
                    <Text style={styles.quickEditText}>
                      Thay đổi thành viên
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.btnQuickEdit}>
                    <ArrowForward
                      style={{
                        height: 20,
                        width: 20,
                        lineHeight: '100%',
                        color: '#fff',
                        paddingHorizontal: 10,
                        paddingVertical: 5,
                      }}
                    />
                    <Text style={styles.quickEditText}>Di chuyển</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.btnQuickEdit}>
                    <Save
                      style={{
                        height: 20,
                        width: 20,
                        lineHeight: '100%',
                        color: '#fff',
                        paddingHorizontal: 10,
                        paddingVertical: 5,
                      }}
                    />
                    <Text style={styles.quickEditText}>Sao chép</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.btnQuickEdit}>
                    <QueryBuilder
                      style={{
                        height: 20,
                        width: 20,
                        lineHeight: '100%',
                        color: '#fff',
                        paddingHorizontal: 10,
                        paddingVertical: 5,
                      }}
                    />
                    <Text style={styles.quickEditText}>
                      Thay đổi ngày hết hạn
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.btnQuickEdit}>
                    <Save
                      style={{
                        height: 20,
                        width: 20,
                        lineHeight: '100%',
                        color: '#fff',
                        paddingHorizontal: 10,
                        paddingVertical: 5,
                      }}
                    />
                    <Text style={styles.quickEditText}>Lưu trữ</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  //  modal
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    flexDirection: 'row',
    borderRadius: 20,
    padding: 15,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    boxShadow: 'none',
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  btnHuy: {
    backgroundColor: 'red',
    paddingHorizontal: 10,
    paddingVertical: 5,
    width: 100,
    borderRadius: 5,
    margin: 5,
  },
  btnCapNhat: {
    backgroundColor: '#61bd4f',
    color: '#fff',
    paddingHorizontal: 10,
    paddingVertical: 5,
    width: 100,
    borderRadius: 5,
    margin: 5,
  },
  quickEditText: {
    clear: 'both',
    color: '#e6e6e6',
    display: 'block',
    float: 'left',
    padding: 6,
    textDecoration: 'none',
    transition: 'transform 85ms ease-in',
  },
  overplay: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    backgroundColor: '#00000047',
  },
  btnQuickEdit: {
    flexDirection: 'row',
    backgroundColor: 'rgba(0,0,0,.6)',
    width: 'max-content',
    borderRadius: 3,
    marginVertical: 2,
    alignItems: 'center',
  },
  ColumnTime: {
    fontSize: 12,
    color: '#828080',
  },
  ColumnTitle: {
    fontSize: 16,
    fontWeight: 500,
    color: '#222',
  },
  textInputContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  textInput: {
    flex: 5,
    minHeight: '7%',
    fontSize: 14,
    padding: 10,
    border: '1px solid #ccc',
    marginRight: 10,
    backgroundColor: '#fff',
  },
  btnAddTask: {
    border: '1px solid #ccc',
    flex: 1,
    borderRadius: 5,
    alignItems: 'center',
    padding: '10px 5px',
  },
});
