import React, {useState, useCallback} from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import Header from '../../components/Header';
import BoardHeader from '../../components/boardHeader';
import Column from '../../components/Column';
import AsyncStorage from '@react-native-community/async-storage';
import ModalEdit from '../../components/modalEdit';

const today = new Date();
const day = today.getDay() + '/' + today.getMonth() + '/' + today.getFullYear();
const time =
  today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
const Time = day + ', ' + time;

const App = () => {
  const [modalVisible, setModalVisible] = useState(false);
  let [textModal, setTextModal] = useState('');
  let [idColumn, setIdColumn] = useState('');
  let [idTask, setIdTask] = useState('');
  let [listTodo, setListTodo] = useState([
    {
      id: 'td0',
      title: 'Việc cần làm',
      time: '04/15/2019, 9:25:35',
      tasks: [],
    },
    {
      id: 'td1',
      title: 'Việc đang làm',
      time: '04/15/2019, 11:25:35',
      tasks: [],
    },
    {
      id: 'td2',
      title: 'Đã hoàn thành',
      time: '04/15/2019, 11:25:35',
      tasks: [],
    },
    {
      id: 'td3',
      title: 'Đéo làm nửa',
      time: '04/15/2019, 11:25:35',
      tasks: [],
    },
  ]);

  const setItem = async () => {
    try {
      await AsyncStorage.setItem('listTodo', JSON.stringify(listTodo));
    } catch (error) {
      // Error saving data
    }
  };

  const getItem = async () => {
    await AsyncStorage.getItem('listTodo', (err, result) => {
      listTodo = JSON.parse(result);
    });
  };
  getItem();

  const onAddItem = useCallback(
    (item, id) => {
      if (item.length > 0) {
        const newTask = {
          id: Math.floor(Math.random() * 1000) + 1,
          content: item,
          time: Time,
        };
        //lấy vị trí cot trong state
        let vitri = 0;
        listTodo.forEach((item, index) => {
          if (id === item.id) {
            vitri = index;
            item.tasks.push(newTask);
            setListTodo([...listTodo]);
            setItem();
          }
        });
      }
    },
    [listTodo, setItem],
  );

  const onDeleteItem = useCallback(
    (columnId, id) => {
      const result = window.confirm('Bạn xóa tui thật hak ???');
      if (result) {
        listTodo.forEach((item, index) => {
          if (columnId === item.id) {
            let vitri = 0;
            item.tasks.forEach((item, index) => {
              if (item.id === id) {
                vitri = index;
              }
            });
            item.tasks.splice(vitri, 1);
            setListTodo([...listTodo]);
            setItem();
          }
        });
      }
    },
    [listTodo, setItem],
  );
  const onEditItem = useCallback(
    (columnId, id) => {
      setModalVisible(true);
      let text = '',
        idCo = '',
        idTa = '';
      listTodo.forEach((item) => {
        if (item.id === columnId) {
          idCo = item.id;
          setIdColumn(idCo);
          item.tasks.forEach((item) => {
            if (item.id === id) {
              idTa = item.id;
              text = item.content;
              setTextModal(text);
              setIdTask(idTa);
            }
          });
        }
      });
    },
    [listTodo],
  );

  const onUpdateItem = useCallback(
    (columnId, id, editText) => {
      const nextListTodo = [...listTodo];
      listTodo.forEach((item, index) => {
        if (columnId === item.id) {
          item.tasks.forEach((item, index) => {
            if (item.id === id) {
              const newContent = (item.content = editText);
              if (newContent.length > 0) {
                setListTodo(nextListTodo);
              }
            }
          });
          setItem();
        }
      });
      setModalVisible(false);
    },
    [listTodo, setItem],
  );

  const CloseModal = () => {
    setModalVisible(false);
    setTextModal('');
  };

  return (
    <View style={styles.container}>
      <Header />

      <View style={styles.main}>
        <BoardHeader />
        <View>
          {
            <FlatList
              horizontal
              data={listTodo}
              keyExtractor={(item) => item.id}
              renderItem={({item}) => (
                <Column
                  id={item.id}
                  title={item.title}
                  time={item.time}
                  listTodo={item}
                  tasks={item.tasks}
                  onAddItem={(item, id) => onAddItem(item, id)}
                  onDeleteItem={(columnId, id) => onDeleteItem(columnId, id)}
                  onEditItem={(columnId, id) => onEditItem(columnId, id)}
                />
              )}
            />
          }
        </View>
      </View>
      <ModalEdit
        onUpdateItem={(columnId, id, editText) =>
          onUpdateItem(columnId, id, editText)
        }
        textModal={textModal}
        idCo={idColumn}
        idTa={idTask}
        listTodo={listTodo}
        onEditItem={(columnId, id) => onEditItem(columnId, id)}
        modalVisible={modalVisible}
        CloseModal={() => CloseModal()}
      />
    </View>
  );
};
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    fontFamily: 'Segoe UI',
  },
  main: {
    marginTop: 5,
    backgroundColor: '#026aa7',
    flex: 1,
    padding: 10,
  },
  mainContent: {
    width: 320,
    border: '1px solid #e1e4e8',
    borderRadius: 5,
    backgroundColor: '#f7f8fe',
    padding: 10,
    height: 'fit-content',
    marginHorizontal: 5,
  },
  numder: {
    lineHeight: 30,
    width: 30,
    backgroundColor: '#5680f9',
    borderRadius: '100%',
    display: 'inline-block',
    textAlign: 'center',
    marginRight: 7,
    color: '#fff',
  },
  name: {
    fontSize: 16,
    fontWeight: 500,
  },
  task: {
    paddingBottom: 20,
    paddingLeft: 10,
    paddingTop: 6,
    borderColor: 'white',
    borderBottomWidth: 1,
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
});
